package comp90015.models;
/* COMP90015: Distributed Systems
 * Chadee Racing
 * Author: Avin Chadee <389017>, Jim Smith <286794>, Patrick Clarke <358519>
 */

public class DataPacket {
	public boolean connected;
	public int totalPlayers;
	public int connectionTime;
	public int countDownTime;
	public int playerID;
	public boolean gameStarted = false;
	public PlayerMove[] pm;
	public int time;
}
