package comp90015.server;
/* COMP90015: Distributed Systems
 * Chadee Racing
 * Author: Avin Chadee <389017>, Jim Smith <286794>, Patrick Clarke <358519>
 */

import java.io.*;
import java.net.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.NoSuchElementException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import comp90015.server.controllers.ServerDataHandler;
import comp90015.server.controllers.ServerController;
import comp90015.models.DataPacket;

/** Main class for deployment of the server
 */

public class Server implements Runnable {

	static DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
	static Date date = new Date();
	private static int totalPlayers;
	public static HashMap<InetAddress, Integer> ipList;
	private String name;
	private String mapName;

	public Server(String name, String mapName) 
	{
		this.name = name;
		this.mapName = mapName;
	}
	/**
	 * deploys the server, creates a thread pool
	 * @param args
	 * @throws IOException
	 */
	@Override
	public void run() {

		ipList = new HashMap<InetAddress, Integer>();
		
		System.out.println("[" + dateFormat.format(date) + "] => Booting Server... ");
		
		DataPacket globalData = new DataPacket();
		ServerDataHandler serverData = new ServerDataHandler(globalData);
		serverData.init_Players();
		//array for receiving a udp packet
		byte[] receiveData = new byte[1024];
		DatagramSocket serverSocket = null;
		try {
			serverSocket = new DatagramSocket(1234);
		} catch (SocketException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		final ExecutorService pool;
		
		printInfo(1234);
		
        //loop through non-loopback interfaces and register them
		Enumeration<NetworkInterface> nets = null;
		try {
			nets = NetworkInterface.getNetworkInterfaces();
		} catch (SocketException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        for (NetworkInterface netint : Collections.list(nets))
			try {
				if (!netint.isLoopback()) {
				//displayInterfaceInformation(netint);
				
				
				InetAddress ipToRegister = null;
					
				//register with the lobby server
				Enumeration<InetAddress> ips= netint.getInetAddresses();
				
				//Register IPv4 addresses unless IPv6 is available
				try
				{
					ipToRegister = ips.nextElement();
				}
				catch (NoSuchElementException e) 
				{
					continue;
					
				}
				
				
				while (ips.hasMoreElements())
				{
					InetAddress ip = ips.nextElement();
					if (ip instanceof Inet4Address)
					{
						ipToRegister = ip;
					}
				}
				
				//call the lobby script here with chosen IP
				URL lobby = null;
				try {
					lobby = new URL("http://23.105.11.111/lobby.py?mode=add&name=" + name +","+ mapName + "&ip=" +ipToRegister.getHostAddress());
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (Exception e) { e.printStackTrace(); }
				BufferedReader in = null;
				try {
					in = new BufferedReader(
					new InputStreamReader(lobby.openStream()));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				String inputLine;
				try {
					while ((inputLine = in.readLine()) != null)
					    System.out.println(inputLine);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					in.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				}
			} catch (SocketException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		
		
		pool = Executors.newFixedThreadPool(4);
		// continue receiving and processing packets
		while (true) {
			DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
			try {
				serverSocket.receive(receivePacket);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			InetAddress ip = receivePacket.getAddress();
			
			if (!ipList.containsKey(ip) && totalPlayers < 4) {
				ipList.put(ip, findActiveID());
				totalPlayers++;
				System.out.println("User has connected " + ip);
			}
			if (ipList.containsKey(ip)) {
				ServerController a = new ServerController(serverSocket, receivePacket, serverData, ipList);
				pool.execute(a);
				ipList = ServerController.ipList;

				    
			} else {
				//send some stuff back
			}
		}
	}

	private static int findActiveID() {
		for(int i = 1; i <= 4; i++) {
			if(!ipList.containsValue(i)){
				return i;
			}
		}	
		return 0;
	}

	/**
	 * gets the IP of the server
	 * @return
	 */
	public static String getIP()
	{
		InetAddress ip;
		try {
			ip = InetAddress.getLocalHost();
			return ip.getHostAddress();
		} catch (UnknownHostException e) {
			return "Unknown IP Address";
		}
	}

	/**
	 * displays information regarding the server
	 * @param port
	 */
	public static void printInfo(int port)
	{
		System.out.println("[" + dateFormat.format(date) + "] => Server successfully running on " + getIP() + " port=" + port);
		System.out.println("[" + dateFormat.format(date) + "] => Press Ctrl+C to cancel process");
		System.out.println("------------");
		System.out.println();
	}
	
	static void displayInterfaceInformation(NetworkInterface netint) throws SocketException {
        System.out.printf("Display name: %s\n", netint.getDisplayName());
        System.out.printf("Name: %s\n", netint.getName());
        Enumeration<InetAddress> inetAddresses = netint.getInetAddresses();
        for (InetAddress inetAddress : Collections.list(inetAddresses)) {
        	System.out.printf("InetAddress: %s\n", inetAddress.toString().substring(1));
        }
        System.out.printf("\n");
     }
}