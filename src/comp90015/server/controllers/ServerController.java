package comp90015.server.controllers;
/* COMP90015: Distributed Systems
 * Chadee Racing
 * Author: Avin Chadee <389017>, Jim Smith <286794>, Patrick Clarke <358519>
 */

import java.io.*;
import java.lang.reflect.Array;
import java.net.*;
import java.util.HashMap;

import comp90015.models.PlayerMove;
import comp90015.models.DataPacket;
import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;

public class ServerController implements Runnable
{
	DatagramSocket s;
	String sentence;
	InetAddress IPAddress;
	int port;
	byte[] sendData = new byte[1024];
	byte[] receiveData = new byte[1024];
	public static HashMap<InetAddress, Integer> ipList = new HashMap<InetAddress, Integer>();

	ServerDataHandler serverData;

	public ServerController(DatagramSocket s, DatagramPacket receivePacket, 
			ServerDataHandler serverData, HashMap<InetAddress, Integer> ipList)
	{
		this.s  = s;
		this.sentence = new String(receivePacket.getData());
		IPAddress = receivePacket.getAddress();
		port = receivePacket.getPort();
		this.serverData = serverData;
		this.ipList = ipList;
	}

	public void run()
	{	
		try {
			//convert to data packet
			DataPacket dp = toObject(sentence);

			if(dp.connected == false){
				ipList.remove(IPAddress);
			}
			else{

				//set player settings
				serverData.transform(dp);	
				//convert back to string
				sendData = toJSON(serverData.get_global_data()).getBytes();
				//System.out.println(toJSON(serverData.get_global_data()));			
				DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, port);



				s.send(sendPacket);
			}
		} catch (IOException ioe) {

		}
		

	}


	public static String toJSON(DataPacket dp){
		return new JSONSerializer().deepSerialize(dp);
	}

	public static DataPacket toObject(String request)
	{
		@SuppressWarnings("rawtypes")
		DataPacket dp = (DataPacket) new JSONDeserializer().use("DataPacket.pm", Array.class).use("DataPacket.pm.values", PlayerMove.class).deserialize(request);

		return dp;
	}	
}