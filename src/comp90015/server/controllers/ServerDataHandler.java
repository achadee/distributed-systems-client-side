package comp90015.server.controllers;
/* COMP90015: Distributed Systems
 * Chadee Racing
 * Author: Avin Chadee <389017>, Jim Smith <286794>, Patrick Clarke <358519>
 */

/**
 * used for setting the server data
 */
import comp90015.models.PlayerMove;
import comp90015.models.DataPacket;

public class ServerDataHandler {

	DataPacket globalData;

	int MAXPLAYERS = 4;
	public PlayerMove[] plm = new PlayerMove[MAXPLAYERS];


	public void init_Players(){
		globalData.pm = plm;
	}


	public ServerDataHandler(DataPacket globalData) {
		this.globalData = globalData;
	}

	//adds 1 to the player count
	public void setTotalPlayers(){
		if (globalData.totalPlayers < MAXPLAYERS){
			globalData.totalPlayers = globalData.totalPlayers + 1;
			System.out.println(globalData.totalPlayers);

		}
	}

	public void transform(DataPacket dp) {
		if (globalData.pm[dp.playerID] == null){
			PlayerMove instance = new PlayerMove();
			globalData.pm[dp.playerID] = instance; 
		}
		globalData.playerID = dp.playerID;
		
		if (dp.pm[dp.playerID].id > globalData.pm[dp.playerID].id){
			globalData.pm[dp.playerID].id = dp.pm[dp.playerID].id;


			globalData.pm[dp.playerID].setX(dp.pm[dp.playerID].getX());
			globalData.pm[dp.playerID].setY(dp.pm[dp.playerID].getY());
			globalData.pm[dp.playerID].setDegrees(dp.pm[dp.playerID].getDegrees());
			globalData.pm[dp.playerID].setRotation(dp.pm[dp.playerID].getRotation());
			globalData.pm[dp.playerID].setVelocity(fixVelocity(dp.pm[dp.playerID].getVelocity()));
			globalData.pm[dp.playerID].setSpeed(dp.pm[dp.playerID].getSpeed());
		}
		else{
			//sockets out of order
		}
	}

	private double fixVelocity(double velocity) {
		if (velocity < 1.8*Math.pow(10, -9) && velocity > -1.88*Math.pow(10, -9)){
			return 0.0;
		}
		else if(velocity <= -1.88*Math.pow(10,-9)){
			return -1.0;
		}
		else{
			return 1.0;
		}
	}


	public DataPacket get_global_data(){
		return globalData;
	}
}
