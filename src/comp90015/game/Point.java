package comp90015.game;
/* COMP90015: Distributed Systems
 * Chadee Racing
 * Author: Avin Chadee <389017>, Jim Smith <286794>, Patrick Clarke <358519>
 */

class Point
{
	public double x;
	public double y;

	public Point(double x, double y)
	{
		this.x = x;
		this.y = y;
	}

	/** Determines the distance between this point and another point.
	 */
	public double distanceTo(double destX, double destY)
	{
		double dist_x = this.x - destX;
		double dist_y = this.y - destY;
		return Math.sqrt(dist_x * dist_x + dist_y * dist_y);
	}

	/** Determines the distance between this point and another point.
	 */
	public double distanceTo(Point dest)
	{
		double dist_x = this.x - dest.x;
		double dist_y = this.y - dest.y;
		return Math.sqrt(dist_x * dist_x + dist_y * dist_y);
	}
}
