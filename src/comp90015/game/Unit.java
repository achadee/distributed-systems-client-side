package comp90015.game;
/* COMP90015: Distributed Systems
 * Chadee Racing
 * Author: Avin Chadee <389017>, Jim Smith <286794>, Patrick Clarke <358519>
 */

import org.newdawn.slick.SlickException;
import org.newdawn.slick.Graphics;

/** A game character, such as a monster or player.
 */
public abstract class Unit extends Actor
{
    // Attributes
    protected double speed;
    protected int hp;
    protected int max_hp;
    protected int collision_damage;
    protected int damage;
    // cooldown is the number of frames till this unit can attack again
    protected int cooldown = 0;
    protected int firepower;
    // dead_time is the number of frames this unit has until it can drive and
    // collide again
    protected int dead_time = 0;

    public boolean isonIce;
    
    // Stores the dialogue string displayed above the unit's head, or null
    private String dialogue = null;
    // Milliseconds before dialogue disappears
    private int dialogue_timer = 0;
    // Milliseconds dialogue appears on the screen
    private static final int DIALOGUE_TIME = 4000;

    // Constructor

    /** Create a new Unit.
     * @param image_path Path of unit's image file.
     * @param hp Initial value of hp and max_hp.
     * @param max_damage Initial value of max_damage.
     * @param firepower Number of firepower upgrades obtained.
     * @param angle Initial value of angle.
     */
    protected Unit(String image_path, int hp,
        int collision_damage,
        int damage, int firepower, double x, double y, Angle angle)
        throws SlickException
    {
        super(image_path, x, y, angle);
        this.speed = 0;
        this.hp = this.max_hp = hp;
        this.collision_damage = collision_damage;
        this.damage = damage;
        this.firepower = firepower;
    }
    
  
    /** The number of pixels-per-millisecond to accelerate per millisecond. */
    public double getAccel()
    {
    	System.out.println(isonIce);
		return isonIce ? 0.0000000000000001 : 0.0005;
    }

    /** The number of radians to rotate per millisecond. */
    public double getRotateSpeed()
    {
        return 0.004;
    }

    /** The number of radians to rotate per millisecond when spinning out of
     * control. */
    public double getSpinSpeed()
    {
        return 0.0008;
    }

    // Getters/setters

    /** The hit points (health) of the unit. */
    public int getHP()
    {
        return hp;
    }

    /** The maximum number of hit points the unit can get. */
    public void setMaxHP(int val)
    {
        max_hp = val;
    }
    public int getMaxHP()
    {
        return max_hp;
    }

    /** The amount of damage dealt by this unit upon collision. */
    public void setCollisionDamage(int val)
    {
        collision_damage = val;
    }
    public int getCollisionDamage()
    {
        return collision_damage;
    }

    /** The amount of damage dealt by this unit's bullets (only relevant if
     * the unit can fire). */
    public void setDamage(int val)
    {
        damage = val;
    }
    public int getDamage()
    {
        return damage;
    }

    /** Decrease the length of time between firing by 80ms
     * (only relevant if the unit can fire). */
    public void increaseFirepower()
    {
        firepower++;
    }
    public int getFirepower()
    {
        return firepower;
    }

    /** The minimum amount of time between firing a bullet, in milliseconds
     * (only relevant if the unit can fire). */
    public int getMaxCooldown()
    {
        return 300 - (80*firepower);
    }

    /** The number of pixels to move per millisecond. */
    public double getSpeed()
    {
        return this.speed;
    }

    /** The unit's coefficient of friction. 1 for normal friction, 0 for no
     * friction. */
    public double getFriction()
    {
        return 1;
    }

    // Abstract methods (need to be overridden) and overridable methods

    /** Perform an AI update. This should only modify the internal state of
     * the Unit. By default, does nothing.
     * @param world The world the player is on (to check blocking).
     * @param player The player character (for AI updates).
     * @param delta Time passed since last frame (milliseconds).
     */
    protected void update_ai(World world, Player player, double delta)
        throws SlickException
    {
    }

    // Fixed methods

    /** Heal the unit to full HP. */
    public void heal()
    {
        hp = max_hp;
    }

    /** Heal the unit by a certain amount of hit points. Does not go past
     * the maximum.
     * @param amount Amount of hit points to heal by.
     */
    public void heal(int amount)
    {
        hp += amount;
        if (hp > max_hp) hp = max_hp;
    }

    /** Wound the unit by a certain amount of hit points. Does not go
     * past 0 (dead).
     * @param amount Amount of hit points to wound by.
     */
    public void wound(int amount)
    {
        hp -= amount;
        if (hp <= 0)
            hp = 0;
    }

    /** Knock out a unit (cannot move or collide for several seconds).
     */
    public void kill()
    {
        dead_time = 700;
    }
    
    /** Determines whether this unit is alive (able to move and collide). */
    public boolean isAlive()
    {
        return dead_time <= 0;
    }

    /** Determines whether the unit is a bullet (and therefore, can't collide
     * with other bullets).
     */
    public boolean isBullet()
    {
        return false;
    }

    /** Determines whether the unit can pick up items.
     */
    public boolean canPickup()
    {
        return false;
    }

    /** Sets the unit's dialogue for 5 seconds. */
    protected void setDialogue(String dialogue, boolean override)
    {
        if (!override && this.dialogue != null)
            return;
        this.dialogue = dialogue;
        this.dialogue_timer = DIALOGUE_TIME;
    }

    /** Draw the Player to the screen at the correct place.
     * @param g The current Graphics context.
     * @param cam_x Camera x position in pixels.
     * @param cam_y Camera y position in pixels.
     */
    public void render(Graphics g, int cam_x, int cam_y)
    {
        float hp_percent = (float) this.hp / this.max_hp;
        render(g, cam_x, cam_y, hp_percent, dialogue);
    }

    /** Run an update on the player (perform AI and move).
     * @param world The world the player is on (to check blocking).
     * @param unit The player character (for AI updates).
     * @param delta Time passed since last frame (milliseconds).
     */
    public void updateu(World world, Unit unit, int delta)
        throws SlickException
    {
    	
    	double temp_x = 0.0;
    	double temp_y = 0.0;
        // Lower the cooldown (so eventually you can attack again)
        if (cooldown > 0)
            // Decrease by the number of milliseconds passed
            cooldown -= delta;
        // Lower the dead and boost time
        if (dead_time > 0)
            dead_time -= delta;
        // Decrease dialogue timer
        if (dialogue_timer > 0)
            // Decrease by the number of milliseconds passed
            dialogue_timer -= delta;
        if (dialogue_timer <= 0)
            dialogue = null;
        // Apply friction
        int tile_x = (int) x / world.getTileWidth();
        int tile_y = (int) y / world.getTileHeight();
        double friction = world.tileFriction(tile_x, tile_y)
            * this.getFriction();
         
        // Currently just reduce speed by some factor (this has the effect of
        // creating a maximum speed for a given coefficient of friction and
        // acceleration)   
        this.speed = this.speed * Math.pow(1 - friction, delta);
        // Move based on speed
        // Calculate the amount to move in each direction
        double amount = delta * this.speed;
        
        if(friction == 0.00000001){
        	isonIce = true;
        }
        else{
        	isonIce = false;
        	temp_x = this.angle.getXComponent(amount);
        	temp_y = this.angle.getYComponent(amount);
        }
        
        double amount_x;
        double amount_y;
        
        if(!isonIce){
        	 amount_x = this.angle.getXComponent(amount);
             amount_y = this.angle.getYComponent(amount);
             moveto(world, this.x + amount_x, this.y + amount_y);
        }
        else{
        	moveto(world, this.x + temp_x, this.y + temp_y);
        	//amount_x = a.getXComponent(amount);
        	//amount_y = a.getYComponent(amount);
        }
        
        
    }

    /** Update the player's x and y coordinates.
     * Prevents the player from moving outside the map space, and also updates
     * the direction the player is facing.
     * @param world The world the player is on (to check blocking).
     * @param x New x coordinate.
     * @param y New y coordinate.
     */
    public void moveto(World world, double x, double y)
    {
        int tile_x = (int) x / world.getTileWidth();
        int tile_y = (int) y / world.getTileHeight();
        if (world.tileBlocks(tile_x, tile_y))
        {
            this.speed = 0;
        }
        else
        {
            Unit collided = world.unitInCollisionRange(x, y, this);
            if (collided != null)
            {
                this.collide(collided);
                collided.collidedBy(this);
            }
            else
            {
                this.x = x;
                this.y = y;
            }
        }
    }

    /** This unit collides with another.
     * Note that this is asymmetrical. 'this' is the unit that crashed into
     * 'other' (slow down 'this' but not 'other').
     * @param other The unit to collide with.
     */
    public void collide(Unit other)
    {
    }

    /** Another unit collided with this actor.
     */
    @Override
    public void collidedBy(Unit other)
    {
        // 'other' loses velocity, but not 'this'.
        // If 'other' rams 'this' from behind, 'other' will stop moving but
        // 'this' can keep driving. But if 'other' backs into 'this''s front,
        // 'other' will stop moving and 'this' will also stop when it tries to
        // move forward. In a head-on collision, both will stop.
        other.speed = 0;
    }
}
