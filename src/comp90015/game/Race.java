package comp90015.game;
/* COMP90015: Distributed Systems
 * Chadee Racing
 * Author: Avin Chadee <389017>, Jim Smith <286794>, Patrick Clarke <358519>
 */

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.net.SocketException;
import java.net.UnknownHostException;

import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Input;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import com.battlebardgames.tinysui.ActionListener;
import com.battlebardgames.tinysui.Button;
import com.battlebardgames.tinysui.UiElement;
import com.battlebardgames.tinysui.container.RootUiElement;

import comp90015.client.DataHandler;
import comp90015.models.DataPacket;

/** Main class for the Shadow Kart Game engine.
 * Handles initialisation, input and rendering.
 */

public class Race extends BasicGameState implements org.newdawn.slick.KeyListener
{
	/** Location of the "assets" directory. */
	public static final String ASSETS_PATH = "assets";

	RootUiElement m_rootUiElement;
	RootUiElement m_rootUiElement2;

	int val = 0;
	//this is a default map name, it should exist in the assets dir 
	public String mapName = "ice_world.tmx";

	/** The game state. */
	private World world;

	boolean correctpositions = false;

	DataHandler client = null;

	/** Screen width, in pixels. */
	public static final int SCREENWIDTH = 800;
	/** Screen height, in pixels. */
	public static final int SCREENHEIGHT = 600;

	/** Create a new Game object. */
	public Race(int state)
	{
	}

	public Race(int state, String mapName)
	{
		this.mapName = mapName;
	}

	/** Initialise the game state.
	 * @param gc The Slick game container object.
	 */
	@Override
	public void init(GameContainer gc, final StateBasedGame sbg)
			throws SlickException
			{
		gc.getGraphics().setBackground(Color.white);
		DataPacket dp = new DataPacket();

		m_rootUiElement = new RootUiElement(gc);
		m_rootUiElement2 = new RootUiElement(gc);

		// Refresh the list of active servers
		Button refreshbutton = new Button(400 - 64, 200, 128, 24);
		refreshbutton.setText("Back to lobby");
		refreshbutton.setClickListener(new ActionListener() {
			@Override
			public void handleEvent(UiElement source) {
				sbg.enterState(Game.multi);
			}
		});

		// Refresh the list of active servers
		Button restartbutton = new Button(650, 10, 128, 24);
		restartbutton.setText("Restart");
		restartbutton.setClickListener(new ActionListener() {
			@Override
			public void handleEvent(UiElement source) {
				if (client.dp.playerID == 0){
					world.units.elementAt(0).x = 1260;
					world.units.elementAt(0).y = 13086;
					world.units.elementAt(0).speed = 0;
					world.units.elementAt(0).angle = new Angle(0);
				}
				else if (client.dp.playerID == 1){
					world.units.elementAt(1).x = 1332;
					world.units.elementAt(1).y = 13086;
					world.units.elementAt(1).speed = 0;
					world.units.elementAt(1).angle = new Angle(0);
				}
				else if (client.dp.playerID == 2){
					world.units.elementAt(2).x = 1404;
					world.units.elementAt(2).y = 13086;
					world.units.elementAt(2).speed = 0;
					world.units.elementAt(2).angle = new Angle(0);
				}
				else if (client.dp.playerID == 3){
					world.units.elementAt(3).x = 1476;
					world.units.elementAt(3).y = 13086;
					world.units.elementAt(3).speed = 0;
					world.units.elementAt(3).angle = new Angle(0);
				}
				world.game_over = false;
			}
		});

		m_rootUiElement2.addChild(restartbutton);

		m_rootUiElement.addChild(refreshbutton);

		try {

			client = new DataHandler(dp);

		} catch (SocketException e) {
			e.printStackTrace();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		//ClientController controller = new ClientController();
		world = new World(client, mapName);
			}

	@Override
	public void keyPressed(int key, char c) {
		if(key == Input.KEY_ESCAPE)
			System.exit(0);
		m_rootUiElement.handleInput(key, c);
		m_rootUiElement2.handleInput(key, c);
	}

	@Override
	public void mouseMoved(int oldx, int oldy, int newx, int newy) {
		m_rootUiElement.mouseMoved(oldx, oldy, newx, newy);
		m_rootUiElement2.mouseMoved(oldx, oldy, newx, newy);
	}

	@Override
	public void mouseDragged(int oldx, int oldy, int newx, int newy) {
		m_rootUiElement.mouseMoved(oldx, oldy, newx, newy);
		m_rootUiElement2.mouseMoved(oldx, oldy, newx, newy);

	}

	@Override
	public void mousePressed(int button, int x, int y) {
		m_rootUiElement.mousePressed(button, x, y);
		m_rootUiElement2.mousePressed(button, x, y);
	}

	@Override
	public void mouseReleased(int button, int x, int y) {
		m_rootUiElement.mouseReleased(button, x, y);
		m_rootUiElement2.mouseReleased(button, x, y);
	}

	@Override
	public void mouseWheelMoved(int change) {
		m_rootUiElement.mouseWheelMoved(change);
		m_rootUiElement2.mouseWheelMoved(change);
	}








	/** Update the game state for a frame.
	 * @param gc The Slick game container object.
	 * @param delta Time passed since last frame (milliseconds).
	 */
	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int delta)
			throws SlickException
			{

		m_rootUiElement.update(gc, delta);
		m_rootUiElement2.update(gc, delta);
		//check if client timed out

		if(client.timeoutCounter > 10){
			sbg.enterState(Game.timeout);
		}


		Input input = gc.getInput();
		input.disableKeyRepeat();
		// Update the player's rotation and position based on key presses.
		double rotate_dir = 0;
		double move_dir = 0;


		try {
			client.DistributeData(client.dp);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}


		if (input.isKeyDown(Input.KEY_DOWN) || input.isKeyDown(Input.KEY_S)){
			move_dir -= 1;

			if(input.isKeyPressed(Input.KEY_DOWN)){
				try {

					client.DistributeData(client.dp);
				} catch (IOException e) {
					e.printStackTrace();
				}

				//set true
			}
		}
		//---
		//if true
		//set false
		if (input.isKeyDown(Input.KEY_UP) || input.isKeyDown(Input.KEY_W)){
			move_dir += 1;
			if(input.isKeyPressed(Input.KEY_UP)){
				try {
					client.DistributeData(client.dp);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}            
		}
		if (input.isKeyDown(Input.KEY_LEFT) || input.isKeyDown(Input.KEY_A)){
			rotate_dir -= 1;

			if(input.isKeyPressed(Input.KEY_LEFT)){
				try {
					client.DistributeData(client.dp);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		if (input.isKeyDown(Input.KEY_RIGHT) || input.isKeyDown(Input.KEY_D)){
			rotate_dir += 1;
			if(input.isKeyPressed(Input.KEY_RIGHT)){
				try {
					client.DistributeData(client.dp);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}




		// Let World.update decide what to do with this data.
		for (int i=0; i<delta; i++)
			world.update(rotate_dir, move_dir, false);






			}

	/** Render the entire screen, so it reflects the current game state.
	 * @param gc The Slick game container object.
	 * @param g The Slick graphics object, used for drawing.
	 */
	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g)
			throws SlickException
			{



		// Let World.render handle the rendering.
		if(client.dp.time < 100 && client.timeoutCounter > 0){
			g.drawRect(150, 400, 500, 10);


			if (client.timeoutCounter <= 10){
				val = client.timeoutCounter;
			}else{
				val = 10;
			}
			g.fillRect(150, 400, val * 50, 10);
		}else{
			world.render(g);

			// Render game over message
			if (world.game_over)
			{
				g.setColor(new Color(1.0f, 1.0f, 1.0f, 0.5f));
				g.fillRect(0, 0, SCREENWIDTH, SCREENHEIGHT);

				m_rootUiElement.render(gc, g);


			}

		}
		m_rootUiElement2.render(gc, g);
			}

	public int getID() {
		return 1;
	}

	public void keyReleased(int key, char c) {
		if (key == Input.KEY_DOWN || key == Input.KEY_UP || key == Input.KEY_LEFT || key == Input.KEY_RIGHT) {
			try {
				client.DistributeData(client.dp);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
