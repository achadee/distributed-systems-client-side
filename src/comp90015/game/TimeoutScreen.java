package comp90015.game;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import com.battlebardgames.tinysui.ActionListener;
import com.battlebardgames.tinysui.Button;
import com.battlebardgames.tinysui.UiElement;
import com.battlebardgames.tinysui.container.RootUiElement;

public class TimeoutScreen extends BasicGameState {
	RootUiElement m_rootUiElement;

	protected Image img = null;
	
	public TimeoutScreen(int timeout) throws SlickException {
		// TODO Auto-generated constructor stub

	}

	@Override
	public void init(GameContainer gc, final StateBasedGame sbg)
			throws SlickException {
		// TODO Auto-generated method stub

		img = new Image("assets/sadface.png");

		
		
		m_rootUiElement = new RootUiElement(gc);

		// Refresh the list of active servers
		Button backbtn = new Button(340, 250, 128, 24);
		backbtn.setText("Ok!");
		backbtn.setClickListener(new ActionListener() {
			@Override
			public void handleEvent(UiElement source) {
				sbg.enterState(Game.multi);
				
				//refreshServerList(); 
				System.out.println("Check for new games");
			}
		}); 
		
		m_rootUiElement.addChild(backbtn);

	}

	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g)
			throws SlickException {
		g.drawImage(img, 30, 250);
		g.drawString("The server timed out!", 315, 200);
		

		m_rootUiElement.render(gc, g);
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int delta)
			throws SlickException {
		m_rootUiElement.update(gc, delta);

	}
	
	
	@Override
	public void mouseMoved(int oldx, int oldy, int newx, int newy) {
		m_rootUiElement.mouseMoved(oldx, oldy, newx, newy);
	}

	@Override
	public void mouseDragged(int oldx, int oldy, int newx, int newy) {
		m_rootUiElement.mouseMoved(oldx, oldy, newx, newy);
	}

	@Override
	public void mousePressed(int button, int x, int y) {
		m_rootUiElement.mousePressed(button, x, y);
	}

	@Override
	public void mouseReleased(int button, int x, int y) {
		m_rootUiElement.mouseReleased(button, x, y);
	}

	@Override
	public void mouseWheelMoved(int change) {
		m_rootUiElement.mouseWheelMoved(change);
	}

	@Override
	public int getID() {
		// TODO Auto-generated method stub
		return 4;
	}

}
