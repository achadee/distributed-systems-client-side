package comp90015.game;
/* COMP90015: Distributed Systems
 * Chadee Racing
 * Author: Avin Chadee <389017>, Jim Smith <286794>, Patrick Clarke <358519>
 */

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

/** An object which can be displayed on the map.
 */
public abstract class Actor
{
	protected Image img = null;

	// In pixels (only used if on the map)
	public double x;
	public double y;
	protected Angle angle;

	// Accessors

	/** The x coordinate of the actor (pixels). */
	public double getX()
	{
		return x;
	}

	/** The y coordinate of the actor (pixels). */
	public double getY()
	{
		return y;
	}

	/** The angle of the actor (radians). */
	public Angle getAngle()
	{
		return angle;
	}

	// Constructor

	/** Create a new Actor.
	 * @param image_path Path of actor's image file.
	 * @param x x coordinate of the actor, in pixels.
	 * @param y y coordinate of the actor, in pixels.
	 */
	protected Actor(String image_path, double x, double y)
			throws SlickException
			{
		img = new Image(image_path);
		this.x = x;
		this.y = y;
		this.angle = new Angle(0.0);
			}

	/** Create a new Actor.
	 * @param image_path Path of actor's image file.
	 * @param x x coordinate of the actor, in pixels.
	 * @param y y coordinate of the actor, in pixels.
	 * @param angle angle of the actor.
	 */
	protected Actor(String image_path, double x, double y, Angle angle)
			throws SlickException
			{
		img = new Image(image_path);
		this.x = x;
		this.y = y;
		this.angle = angle;
			}

	// Abstract methods (need to be overridden)

	/** The actor's display name. */
	public abstract String getName();

	/** Determines the distance between a this actor and a point.
	 */
	public double distanceTo(double destX, double destY)
	{
		double dist_x = x - destX;
		double dist_y = y - destY;
		return Math.sqrt(dist_x * dist_x + dist_y * dist_y);
	}

	/** Determines the distance between this actor and another.
	 */
	public double distanceTo(Actor target)
	{
		double dist_x = x - target.getX();
		double dist_y = y - target.getY();
		return Math.sqrt(dist_x * dist_x + dist_y * dist_y);
	}

	/** Determines whether this actor is collided with another.
	 */
	public boolean collidedWith(Actor target)
	{
		return distanceTo(target) < World.INTERACT_DISTANCE;
	}

	/** Another unit collided with this actor.
	 */
	public void collidedBy(Unit other)
	{
	}

	/** Determines whether this actor is within the camera bounds.
	 */
	public boolean onScreen(World world)
	{
		Camera cam = world.getCamera();
		return x >= cam.getLeft() && x <= cam.getRight() &&
				y >= cam.getTop() && y <= cam.getBottom();
	}

	/** Draw the actor to the screen at the correct place.
	 * @param g The current Graphics context.
	 * @param image The image to render.
	 * @param cam_x Camera x position in pixels.
	 * @param cam_y Camera y position in pixels.
	 * @param hp_percent The percentage HP to show in the bar.
	 */
	protected void render(Graphics g, int cam_x, int cam_y, float hp_percent)
	{
		render(g, cam_x, cam_y, hp_percent, null);
	}

	/** Draw the actor to the screen at the correct place.
	 * @param g The current Graphics context.
	 * @param image The image to render.
	 * @param cam_x Camera x position in pixels.
	 * @param cam_y Camera y position in pixels.
	 * @param hp_percent The percentage HP to show in the bar.
	 * @param dialogue Dialogue to show above the HP bar, or null.
	 */
	protected void render(Graphics g, int cam_x, int cam_y, float hp_percent,
			String dialogue)
	{
		//edit shit in here!!
		// Panel colours
		Color VALUE = new Color(1.0f, 1.0f, 1.0f);          // White
		Color BAR_BG = new Color(0.0f, 0.0f, 0.0f, 0.8f);   // Black, transp

		int hp_bar_height = 18;
		int dialogue_bar_y_offset = -30;
		// Calculate the player's on-screen location from the camera
		int screen_x, screen_y;
		screen_x = (int) (this.x - cam_x) - (this.img.getWidth()/2);
		screen_y = (int) (this.y - cam_y) - (this.img.getHeight()/2);
		this.drawImage(screen_x, screen_y, this.angle);
		if (dialogue != null)
		{
			int text_width = g.getFont().getWidth(dialogue);
			int text_x = (int) (this.x - cam_x) - (text_width/2);
			int bar_width = text_width + 6;
			int bar_x = (int) (this.x - cam_x) - (bar_width/2);
			int bar_y = screen_y + dialogue_bar_y_offset;
			g.setColor(BAR_BG);
			g.fillRect(bar_x, bar_y, bar_width, hp_bar_height);
			g.setColor(VALUE);
			g.drawString(dialogue, text_x, bar_y);
		}
	}

	/** Draw the actor's image to the screen at a given rotation and place.
	 * @param x X position to draw to in screen pixels.
	 * @param y Y position to draw to in screen pixels.
	 * @param rotation Angle to draw the image at, or null for default.
	 */

	public void drawImage(int x, int y, Angle rotation)
	{
		img.setRotation(this.angle != null ? (float) (this.angle.getDegrees())
				: 0);
		img.draw(x, y);
	}
}
