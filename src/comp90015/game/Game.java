package comp90015.game;

import java.net.InetAddress;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

public class Game extends StateBasedGame {

	public static final int menu = 0;
	public static final int race = 1;
	public static final int multi = 2;
	public static final int create = 3;
	public static final int timeout = 4; 

	public static InetAddress ip;

	/** Screen width, in pixels. */
	public static final int SCREENWIDTH = 800;
	/** Screen height, in pixels. */
	public static final int SCREENHEIGHT = 600;

	public Game(String name) throws SlickException {
		super(name);
		this.addState(new Menu(menu));
		this.addState(new Multiplayer(multi));
		this.addState(new Create(create));
		this.addState(new TimeoutScreen(timeout));
	}

	@Override
	public void initStatesList(GameContainer gc) throws SlickException {
		this.getState(menu).init(gc, this);
		this.getState(multi).init(gc, this);
		this.getState(create).init(gc, this);
		this.getState(timeout).init(gc, this);
		
		this.enterState(menu);
	}

	/** Start-up method. Creates the game and runs it.
	 * @param args Command-line arguments (ignored).
	 */
	public static void gameStart()
			throws SlickException
			{
		AppGameContainer app = new AppGameContainer(new Game("Jim presents Chadee Clarke Racing"));
		// setShowFPS(true), to show frames-per-second.
		app.setDisplayMode(SCREENWIDTH, SCREENHEIGHT, false);
		app.setShowFPS(false);
		//app.setFullscreen(true);
		app.start();
		
		
			}

	public static InetAddress getIp() {
		return ip;
	}

	public static void setIp(InetAddress ip) {
		Game.ip = ip;
	}
}
