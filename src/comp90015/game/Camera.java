package comp90015.game;
/* COMP90015: Distributed Systems
 * Chadee Racing
 * Author: Avin Chadee <389017>, Jim Smith <286794>, Patrick Clarke <358519>
 */

/** The camera, a rectangle positioned in the world.
 */
public class Camera
{
	// In pixels
	protected double left;
	protected double top;

	// Accessors

	/** The left x coordinate of the camera (pixels). */
	public double getLeft()
	{
		return left;
	}
	/** The right x coordinate of the camera (pixels). */
	public double getRight()
	{
		return left + Race.SCREENWIDTH;
	}
	/** The top y coordinate of the camera (pixels). */
	public double getTop()
	{
		return top;
	}
	/** The bottom y coordinate of the camera (pixels). */
	public double getBottom()
	{
		return top + Race.SCREENHEIGHT;
	}

	/** Creates a new Camera centered around the player.
	 * @param world The world, to calculate the map dimensions.
	 * @param player The player, to get the player's location.
	 */
	public Camera(World world, Unit player)
	{
		moveto(world, player);
	}

	/** Move the camera such that the given player is centered.
	 * @param world The world, to calculate the map dimensions.
	 * @param player The player, to get the player's location.
	 */
	public void moveto(World world, Unit player)
	{
		double left = player.getX() - (Race.SCREENWIDTH / 2);
		double top = player.getY() - (Race.SCREENHEIGHT / 2);
		moveto(world, left, top);
	}

	/** Update the camera's x and y coordinates.
	 * Prevents the camera from moving outside the map space.
	 * @param world The world the camera is on (to check blocking).
	 * @param left New left x coordinate of the camera (pixels).
	 * @param top New top y coordinate of the camera (pixels).
	 */
	public void moveto(World world, double left, double top)
	{
		this.left = left;
		this.top = top;
	}
}
