package comp90015.game;
/* COMP90015: Distributed Systems
 * Chadee Racing
 * Author: Avin Chadee <389017>, Jim Smith <286794>, Patrick Clarke <358519>
 */


import java.io.File;
import java.io.FilenameFilter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Iterator;
import java.util.Map;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import com.battlebardgames.tinysui.ActionListener;
import com.battlebardgames.tinysui.Button;
import com.battlebardgames.tinysui.Label;
import com.battlebardgames.tinysui.TextBox;
import com.battlebardgames.tinysui.UiElement;
import com.battlebardgames.tinysui.container.RootUiElement;
import com.battlebardgames.tinysui.container.Window;
import com.battlebardgames.tinysui.list.BasicListItem;
import com.battlebardgames.tinysui.list.ListBox;

import comp90015.server.Server;



public class Create extends BasicGameState {
	
	public Create(int state) {
	}
	
	private RootUiElement m_rootUiElement;

	@Override
	public void init(final GameContainer gc, final StateBasedGame sbg)
			throws SlickException {
		m_rootUiElement = new RootUiElement(gc);
		
		
		
		
		
		Window window1 = new Window(220, 125, 360, 350);
		window1.setTitle("Create a new game:");
		window1.setDraggable(false);
		m_rootUiElement.addChild(window1);

		
		Label nameLbl = new Label(20, 15, 200, 24);
		nameLbl.setText("Enter a name for the game server:");
		window1.addChild(nameLbl);
		
		final TextBox hostname = new TextBox(20, 40, 220, 24);
		hostname.setText("localhost");
		hostname.setEnabled(true);
		window1.addChild(hostname);
		
		
		Label mapLbl = new Label(20, 90, 200, 24);
		mapLbl.setText("Select a map:");
		window1.addChild(mapLbl);
		
		final ListBox listBox = new ListBox(20, 115, 300, 100);

		//gets all .tmx files in the assets directory
		File dir = new File(Race.ASSETS_PATH);
		File[] files = dir.listFiles(new FilenameFilter() {
		    public boolean accept(File dir, String name) {
		        return name.toLowerCase().endsWith(".tmx");
		    }
		});
		
		for (File file : files)
		{
			BasicListItem listItem = new BasicListItem(file.getName(), file.getName());
			listBox.addItem(listItem);
		}

		
		window1.addChild(listBox);
		
		
		final Button multibutton = new Button(180 - 64, 250, 128, 24);
		multibutton.setText("Create");
		multibutton.setClickListener(new ActionListener() {
            @Override
            public void handleEvent(UiElement source) {
            	

        		
        		try {
					Game.setIp(InetAddress.getByName("localhost"));
				} catch (UnknownHostException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
        		Server localserver = null;
        		
        		if(listBox.getSelectedItem() == null)
        		{
        			//if no item is selected just choose first map in the list
        		
        			localserver = new Server(hostname.getText(), (String)((BasicListItem)listBox.getChildren().get(0)).getValue());
        			sbg.addState(new Race(Game.race, (String)((BasicListItem)listBox.getChildren().get(0)).getValue()));
        		
        		}
        		else
        		{
        			//choose the selected map and initialize the race object accordingly
        			
        			localserver = new Server(hostname.getText(), (String) listBox.getSelectedItem().getValue());
        			sbg.addState(new Race(Game.race, (String) listBox.getSelectedItem().getValue()));
        		}
				
				try {
					
	            	 
	        		Thread myThread = new Thread(localserver);
	        		myThread.setDaemon(true); // important, otherwise JVM does not exit at end of main()
	        		myThread.start(); 
	        		System.out.println("Game Started");
					
					sbg.getState(Game.race).init(gc, sbg);
				} catch (SlickException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            	sbg.enterState(Game.race);
            }
        });
		
		window1.addChild(multibutton);
		
		
		// Go back to the main menu
				final Button backbutton = new Button(180 - 64, 280, 128, 24);
				backbutton.setText("Back");
				backbutton.setClickListener(new ActionListener() {
					@Override
					public void handleEvent(UiElement source) {
						sbg.enterState(Game.multi); 
					}
				});
				window1.addChild(backbutton);
        
	}
	
	
	@Override
    public void keyPressed(int key, char c) {
            if(key == Input.KEY_ESCAPE)
                    System.exit(0);
            m_rootUiElement.handleInput(key, c);
    }
	
	@Override
    public void mouseMoved(int oldx, int oldy, int newx, int newy) {
            m_rootUiElement.mouseMoved(oldx, oldy, newx, newy);
    }
    
    @Override
    public void mouseDragged(int oldx, int oldy, int newx, int newy) {
            m_rootUiElement.mouseMoved(oldx, oldy, newx, newy);
    }
    
    @Override
    public void mousePressed(int button, int x, int y) {
            m_rootUiElement.mousePressed(button, x, y);
    }
    
    @Override
    public void mouseReleased(int button, int x, int y) {
            m_rootUiElement.mouseReleased(button, x, y);
    }
    
    @Override
    public void mouseWheelMoved(int change) {
            m_rootUiElement.mouseWheelMoved(change);
    }

	@Override
	public void render(GameContainer gc, final StateBasedGame sbg, Graphics g)
			throws SlickException {

		m_rootUiElement.render(gc, g);
		
		
		
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int delta)
			throws SlickException {
		m_rootUiElement.update(gc, delta);
	}

	@Override
	public int getID() {
		return 3;
	}

}
