package comp90015.game;
/* COMP90015: Distributed Systems
 * Chadee Racing
 * Author: Avin Chadee <389017>, Jim Smith <286794>, Patrick Clarke <358519>
 */

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.simple.parser.ContainerFactory;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import com.battlebardgames.tinysui.ActionListener;
import com.battlebardgames.tinysui.Button;
import com.battlebardgames.tinysui.Label;
import com.battlebardgames.tinysui.TextBox;
import com.battlebardgames.tinysui.UiElement;
import com.battlebardgames.tinysui.container.RootUiElement;
import com.battlebardgames.tinysui.container.Window;
import com.battlebardgames.tinysui.list.BasicListItem;
import com.battlebardgames.tinysui.list.ListBox;
import comp90015.server.Server;

public class Multiplayer extends BasicGameState  {

	List<String> serverList = new ArrayList<String>();
	private RootUiElement m_rootUiElement;

	public Multiplayer(int state) {	}



	public static void checkHosts(String ip) throws UnknownHostException, IOException{
		// TODO
	}


	@Override
	public void init(final GameContainer gc, final StateBasedGame sbg)
			throws SlickException {
		try {
			Multiplayer.checkHosts(InetAddress.getLocalHost().toString());
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		m_rootUiElement = new RootUiElement(gc);
		Window window1 = new Window(32, 32, 600, 500);
		window1.setTitle("Games currently active in network:");
		window1.setDraggable(true);
		m_rootUiElement.addChild(window1);

		gc.getGraphics().drawRect(32, 532, 800, 50);

		Window window2 = new Window(0,400, 600, 76);
		window2.setTitle("Manual connection");
		window2.setDraggable(true);
		window1.addChild(window2);

		ListBox listBox = new ListBox(30, 30, 530, 250);



		Iterator iter = getServerIter();
		
		while(iter != null && iter.hasNext()){
			Map.Entry entry = (Map.Entry)iter.next();
			BasicListItem listItem = new BasicListItem(((String)entry.getValue()).split(",")[0]+ " : "+((String)entry.getValue()).split(",")[1] + " : " + (String)entry.getKey(), (String) entry.getKey()+","+((String)entry.getValue()).split(",")[1]);
			listBox.addItem(listItem);
		}

		window1.addChild(listBox);

		Label pingInfo = new Label(20, 15, 200, 24);
		pingInfo.setText("Enter your IP Address:");
		window2.addChild(pingInfo);

		final TextBox ping = new TextBox(230, 15, 220, 24);
		ping.setText("localhost");
		ping.setEnabled(true);
		window2.addChild(ping);

		// Refresh the list of active servers
		Button refreshbutton = new Button(650, 32, 128, 24);
		refreshbutton.setText("Refresh");
		refreshbutton.setClickListener(new ActionListener() {
			@Override
			public void handleEvent(UiElement source) {
				try {
					init(gc,sbg);
				} catch (SlickException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//refreshServerList(); 
				System.out.println("Check for new games");
			}
		});
		m_rootUiElement.addChild(refreshbutton);

		// Join game button
		Button joinbtn = new Button(650, 60, 128, 24);
		joinbtn.setText("Join Game");

		joinbtn.setClickListener(new ActionListener() {
			@Override
			public void handleEvent(UiElement source) {
				System.out.println("Join the selected game");

				//should find a better way of getting the textbox
				ListBox lb = (ListBox)m_rootUiElement.getChildren().get(0).getChildren().get(1);
				if (lb.getSelectedItem() != null) {

					String remoteIp = ((String) lb.getSelectedItem().getValue()).split(",")[0];
					String map = ((String) lb.getSelectedItem().getValue()).split(",")[1];
					try {
						System.out.println(remoteIp);
						Game.setIp(InetAddress.getByName(remoteIp));
					} catch (UnknownHostException e) {
						System.out.println("Request Server Button");
					}
					try {
						sbg.addState(new Race(Game.race, map));
						sbg.getState(Game.race).init(gc, sbg);
					} catch (SlickException e) {
						e.printStackTrace();
					}
					sbg.enterState(Game.race);

				}
			}
		});
		m_rootUiElement.addChild(joinbtn);

		// Go back to the main menu
		Button backbutton = new Button(650, 88, 128, 24);
		backbutton.setText("Back");
		backbutton.setClickListener(new ActionListener() {
			@Override
			public void handleEvent(UiElement source) {
				sbg.enterState(Game.menu); 
			}
		});
		m_rootUiElement.addChild(backbutton);

		// Exit the game
		Button exitBtn = new Button(32, 550, 128, 24);
		exitBtn.setText("Exit");
		exitBtn.setClickListener(new ActionListener() {
			@Override
			public void handleEvent(UiElement source) {
				System.exit(0);
			}
		});
		m_rootUiElement.addChild(exitBtn);

		// Request the server
		Button requestServerbtn = new Button(460, 15, 128, 24);
		requestServerbtn.setText("Request Server");
		requestServerbtn.setClickListener(new ActionListener() {
			@Override
			public void handleEvent(UiElement source) {
				try {
					System.out.println(ping.getText());
					Game.setIp(InetAddress.getByName(ping.getText()));
				} catch (UnknownHostException e) {
					System.out.println("Request Server Button");
				}
				try {
					sbg.addState(new Race(Game.race));
					sbg.getState(Game.race).init(gc, sbg);
				} catch (SlickException e) {
					e.printStackTrace();
				}
				sbg.enterState(Game.race);
			}
		});
		window2.addChild(requestServerbtn);

		// Create a new game
		Button createbtn = new Button(650, 550, 128, 24);
		createbtn.setText("Create Game");
		m_rootUiElement.addChild(createbtn);
		createbtn.setClickListener(new ActionListener() {
			@Override
			public void handleEvent(UiElement source) {
				System.out.println("Create a game");
				
				sbg.enterState(Game.create);
				
			}
		});
	}

	@Override
	public void keyPressed(int key, char c) {
		if(key == Input.KEY_ESCAPE)
			System.exit(0);
		m_rootUiElement.handleInput(key, c);
	}

	@Override
	public void mouseMoved(int oldx, int oldy, int newx, int newy) {
		m_rootUiElement.mouseMoved(oldx, oldy, newx, newy);
	}

	@Override
	public void mouseDragged(int oldx, int oldy, int newx, int newy) {
		m_rootUiElement.mouseMoved(oldx, oldy, newx, newy);
	}

	@Override
	public void mousePressed(int button, int x, int y) {
		m_rootUiElement.mousePressed(button, x, y);
	}

	@Override
	public void mouseReleased(int button, int x, int y) {
		m_rootUiElement.mouseReleased(button, x, y);
	}

	@Override
	public void mouseWheelMoved(int change) {
		m_rootUiElement.mouseWheelMoved(change);
	}

	@Override
	public void render(GameContainer gc, StateBasedGame arg1, Graphics g)
			throws SlickException {
		g.setBackground(Color.white);
		m_rootUiElement.render(gc, g);
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int delta)
			throws SlickException {
		m_rootUiElement.update(gc, delta);


	}

	@Override
	public int getID() {
		return 2;
	}


	public void refreshServerList() {


	}

	public Iterator getServerIter() {

		try {

			//get lobby server JSON
			URL lobby = new URL("http://23.105.11.111/lobby.py?mode=get");
			BufferedReader in = new BufferedReader(
					new InputStreamReader(lobby.openStream()));

			JSONParser parser = new JSONParser();
			ContainerFactory containerFactory = new ContainerFactory(){
				public List creatArrayContainer() {
					return new LinkedList();
				}
				public Map createObjectContainer() {
					return new LinkedHashMap();
				}

			};

			Map json = (Map)parser.parse(in, containerFactory);
			return json.entrySet().iterator();



		}
		catch(FileNotFoundException fnf)
		{
			
			System.out.println("Server not found or is offline");
		}
		
		catch(ParseException pe)
		{
			System.out.println(pe);
		} 
		catch (MalformedURLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}
}
