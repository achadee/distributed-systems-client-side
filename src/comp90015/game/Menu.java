package comp90015.game;
/* COMP90015: Distributed Systems
 * Chadee Racing
 * Author: Avin Chadee <389017>, Jim Smith <286794>, Patrick Clarke <358519>
 */

import java.awt.Font;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import com.battlebardgames.tinysui.ActionListener;
import com.battlebardgames.tinysui.Button;
import com.battlebardgames.tinysui.Label;
import com.battlebardgames.tinysui.UiElement;
import com.battlebardgames.tinysui.container.RootUiElement;

public class Menu extends BasicGameState {
	
	public Menu(int state) {
	}
	
	private RootUiElement m_rootUiElement;

	@Override
	public void init(GameContainer gc, final StateBasedGame sbg)
			throws SlickException {
		m_rootUiElement = new RootUiElement(gc);
		
		final Button multibutton = new Button(400 - 64, 250, 128, 24);
		multibutton.setText("Multiplayer");
		multibutton.setClickListener(new ActionListener() {
            @Override
            public void handleEvent(UiElement source) {
            	sbg.enterState(Game.multi);
            }
        });
		
        m_rootUiElement.addChild(multibutton);
        
        Button extbtn = new Button(400 - 64, 278, 128, 24);
        extbtn.setText("Exit");
        
        extbtn.setClickListener(new ActionListener() {
            @Override
            public void handleEvent(UiElement source) {
            	System.exit(0);
            	
            }
        });
        
        m_rootUiElement.addChild(extbtn);
		
        Button setbtn = new Button(650, 550, 128, 24);
        setbtn.setText("Settings");
        
        setbtn.setClickListener(new ActionListener() {
            @Override
            public void handleEvent(UiElement source) {
            	System.exit(0);
            	
            }
        });
        
        m_rootUiElement.addChild(setbtn);
	}
	
	
	@Override
    public void keyPressed(int key, char c) {
            if(key == Input.KEY_ESCAPE)
                    System.exit(0);
            m_rootUiElement.handleInput(key, c);
    }
	
	@Override
    public void mouseMoved(int oldx, int oldy, int newx, int newy) {
            m_rootUiElement.mouseMoved(oldx, oldy, newx, newy);
    }
    
    @Override
    public void mouseDragged(int oldx, int oldy, int newx, int newy) {
            m_rootUiElement.mouseMoved(oldx, oldy, newx, newy);
    }
    
    @Override
    public void mousePressed(int button, int x, int y) {
            m_rootUiElement.mousePressed(button, x, y);
    }
    
    @Override
    public void mouseReleased(int button, int x, int y) {
            m_rootUiElement.mouseReleased(button, x, y);
    }
    
    @Override
    public void mouseWheelMoved(int change) {
            m_rootUiElement.mouseWheelMoved(change);
    }

	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g)
			throws SlickException {
		m_rootUiElement.render(gc, g);
		g.setBackground(Color.white);
		g.setColor(Color.black);
		g.drawString("Jim Presents Chadee Clarke Racing!", 250, 200);
	
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int delta)
			throws SlickException {
		m_rootUiElement.update(gc, delta);
	}

	@Override
	public int getID() {
		return 0;
	}

}
