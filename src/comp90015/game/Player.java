package comp90015.game;
/* COMP90015: Distributed Systems
 * Chadee Racing
 * Author: Avin Chadee <389017>, Jim Smith <286794>, Patrick Clarke <358519>
 */

import org.newdawn.slick.SlickException;

/** The character which the player plays as.
 */
public class Player extends Unit
{
    /** Creates a new Player.
     * @param image_path Path of player's image file.
     */
    public Player(String image_path, double x, double y)
        throws SlickException
    {
        // Start off with 100 hit points, 10 collision damage,
        //  8 bullet damage, 0 firepower (300ms cooldown),
        //  at (828,2772), angle 180 degrees.
        super(image_path, 100, 10, 8, 0, x, y, Angle.fromDegrees(0));
    }

    /** Determines whether the unit can pick up items.
     */
    @Override
    public boolean canPickup()
    {
        return true;
    }

    /** Move the player in a given direction.
     * Prevents the player from moving outside the map space, and also updates
     * adds the additional automatic movement of the player.
     * Also applies automatic updates.
     * @param world The world the player is on (to check blocking).
     * @param rotate_dir The player's direction of rotation
     *      (-1 for anti-clockwise, 1 for clockwise, or 0).
     * @param move_dir The player's movement in the car's axis (-1, 0 or 1).
     * @param delta Time passed since last frame (milliseconds).
     * @throws SlickException 
     */
    public void update(World world, double rotate_dir, double move_dir,
        int delta) throws SlickException
    {
        /* Modify the player's rotation */
        Angle rotateamount = new Angle(rotate_dir * delta * getRotateSpeed());
        this.angle = this.angle.add(rotateamount);
        /* Modify the player's speed */
        this.speed += (isonIce ? 0.000001 : move_dir * 0.0005) * delta;
        //moveby(world, amount_x, amount_y);
        updateu(world, this, delta);
    }

    // Y coordinates. These serve as "checkpoints". Once the player is past
    // each of these points, they will respawn at that Y coordinate once dead.
    // Must be in ascending order. Note that the checkpoints will be visited
    // in reverse order, since the player starts at a high Y coordinate.
    static double[] respawn_locations = {2844, 5796, 7812, 9756, 13716};

    /** Cause the player to die (healing and moving the player to a new
     * location).
     * @param world The world the player is on (to check blocking).
     */
    public void death(World world)
    {
        // Heal and reset position
        heal();
        // Center the player horizontally
        double newx = world.getWidth() / 2;
        // Vertically, choose the furthest respawn location which is greater
        // than (closer to the start than) the player's position
        double newy = respawn_locations[respawn_locations.length-1];
        for (double loc : respawn_locations)
        {
            if (loc >= getY())
            {
                newy = loc;
                break;
            }
        }
        moveto(world, newx, newy);
    }

    @Override
    public String getName()
    {
        return "You";
    }
}
