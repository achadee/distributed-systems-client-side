package comp90015.game;
/* COMP90015: Distributed Systems
 * Chadee Racing
 * Author: Avin Chadee <389017>, Jim Smith <286794>, Patrick Clarke <358519>
 */

import java.util.Vector;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tiled.TiledMap;

import comp90015.client.*;

/** Represents the entire game world.
 * (Designed to be instantiated just once for the whole game).
 */
public class World
{
	Player player = null;
	Panel panel = null;
	TiledMap map = null;
	Vector<Player> units;
	Camera camera;

	public double temp_x;
	public double temp_y;
	
	public boolean correct_units_positions =  true;

	DataHandler client;

	/** The maximum distance two actors must be to interact. */
	public static final double INTERACT_DISTANCE = 40;

	public boolean startCountDown = false;

	public boolean game_over = false;
	/** Only meaningful if game over. The player's position at the finish
	 * line. */
	private int player_finish_position = 0;

	public Camera getCamera()
	{
		return camera;
	}

	public int getMapWidth()
	{
		return map.getWidth();
	}

	public int getMapHeight()
	{
		return map.getHeight();
	}

	public int getTileWidth()
	{
		return map.getTileWidth();
	}

	public int getTileHeight()
	{
		return map.getTileHeight();
	}

	/** Get the width of the game world in pixels. */
	public int getWidth()
	{
		return map.getWidth() * map.getTileWidth();
	}

	/** Get the height of the game world in pixels. */
	public int getHeight()
	{
		return map.getHeight() * map.getTileHeight();
	}

	/** Create a new World object. */
	public World(DataHandler client, String mapName)
			throws SlickException
			{
		map = new TiledMap(Race.ASSETS_PATH + "/"+ mapName, Race.ASSETS_PATH);
		this.client = client;
		panel = new Panel();

		// Initialise all of the characters
		units = new Vector<Player>();
		create_units(units);
		units.add(player);

		camera = new Camera(this, units.elementAt(client.getPlayerId()));
			}

	/** Adds all of the game's units to a vector.
	 * Does not include the player.
	 * @param units2 Vector of Unit objects, to append to.
	 */
	private void create_units(Vector<Player> units)
			throws SlickException
			{
		units.add(new Player(Race.ASSETS_PATH + "/karts/elephant.png", 1260, 13086));
		units.add(new Player(Race.ASSETS_PATH + "/karts/donkey.png", 1332, 13086));
		units.add(new Player(Race.ASSETS_PATH + "/karts/dog.png", 1404, 13086));
		units.add(new Player(Race.ASSETS_PATH + "/karts/octopus.png", 1476, 13086));

		//assigns players to client instances
		client.set_players(units);
			}

	/** Adds a unit into the world. */
	public void addUnit(Player unit)
	{
		units.add(unit);
	}

	/** True if the camera has reached the top of the map. */
	public boolean reachedTop()
	{
		return camera.getTop() <= 0;
	}

	/** Update the game state for a frame.
	 * @param rotate_dir The player's direction of rotation
	 *      (-1 for anti-clockwise, 1 for clockwise, or 0).
	 * @param move_dir The player's movement in the car's axis (-1, 0 or 1).
	 * @param use_item Whether the player is using an item this frame.
	 */
	public void update(double rotate_dir, double move_dir, boolean use_item)
			throws SlickException
			{

		// Check if the game is over
		if (!game_over && units.elementAt(client.getPlayerId()).getY() < 1026)
		{
			game_over = true;
			player_finish_position = unitPosition(units.elementAt(client.getPlayerId()));
		}
		if (game_over)
		{
			// Player can no longer control the game
			rotate_dir = 0;
			move_dir = 0;
			use_item = false;
		}


		// Rotate and move the player by rotate_dir and move_dir
		if (units.elementAt(client.getPlayerId()).isAlive() && true){
			units.elementAt(client.getPlayerId()).update(this, rotate_dir, move_dir, 1);

			client.set_play(units.elementAt(client.getPlayerId()), client.getPlayerId(), rotate_dir, move_dir);


			//move other players based on datapacket
			for (int i = 0; i < client.getTotalplayers(); i++){
				if(client.dp.pm[i] == null){
					//do nothing player data not there
				}
				else{
					if(i != client.getPlayerId()){
						units.elementAt(i).update(this, client.dp.pm[i].getRotation(), client.dp.pm[i].getVelocity(), 1);
					units.elementAt(i).speed = client.dp.pm[i].getSpeed();
						units.elementAt(i).angle = new Angle(client.dp.pm[i].getDegrees() * Math.PI / 180f);
					}
					
					if(correct_units_positions){
						if (temp_x != client.dp.pm[i].getX() && temp_y != client.dp.pm[i].getY()){
							units.elementAt(i).x = client.dp.pm[i].getX();
							units.elementAt(i).y = client.dp.pm[i].getY();

							temp_x = client.dp.pm[i].getX();
							temp_y = client.dp.pm[i].getY();
						}
					}
				}
			}
		}

		// Update the camera based on the player's position
		camera.moveto(this, units.elementAt(client.getPlayerId()));

		//TODO - fix
		if(client.getConnectiontime() >= 0){
			client.getConnectiontime();
		}
		if(startCountDown){
			client.getCountdown();
		}
			}

	/** Determine which unit is in collision range of a given point.
	 * May return null.
	 * @param ignore Unit to ignore (i.e., the unit checking collisions).
	 */
	public Unit unitInCollisionRange(double x, double y, Unit ignore)
	{


		for (int i = 0; i< units.size() -1 ; i++)
		{
			if (units.elementAt(i) != ignore && units.elementAt(i).distanceTo(x, y) < INTERACT_DISTANCE)
			{
				return units.elementAt(i);
			}
		}
		return null;
	}

	/** Compute the current position of a given unit.
	 */
	public int unitPosition(Unit unit)
	{
		int unitpos = 1;
		//		for (Unit u : units)
		//		{
		//			if (!u.isBullet() && u.getY() < unit.getY())
		//				unitpos++;
		//		}
		return unitpos;
	}

	/** Render the entire screen, so it reflects the current game state.
	 * @param g The Slick graphics object, used for drawing.
	 * @param textrenderer A TextRenderer object.
	 */
	public void render(Graphics g)
			throws SlickException
			{
		// Calculate the camera location (in tiles) and offset (in pixels)
		int cam_tile_x = (int) camera.getLeft() / map.getTileWidth();
		int cam_offset_x = (int) camera.getLeft() % map.getTileWidth();
		int cam_tile_y = (int) camera.getTop() / map.getTileHeight();
		int cam_offset_y = (int) camera.getTop() % map.getTileHeight();
		int screen_tilewidth = 24;
		int screen_tileheight = 18;
		map.render(-cam_offset_x, -cam_offset_y, cam_tile_x, cam_tile_y,
				screen_tilewidth, screen_tileheight);
		units.elementAt(client.getPlayerId()).render(g, (int) camera.getLeft(), (int) camera.getTop());

		for(int i = 0; i < client.getTotalplayers(); i++){
			if ( i != client.getPlayerId()){
				//define logic to render

				units.elementAt(i).render(g, (int) camera.getLeft(), (int) camera.getTop());
			}
		}



		if (client.getTotalplayers() < 4){
			g.drawString("waiting for players to connect...", 290, 200);
			g.drawString("" + (int)(float)(client.getConnectiontime()/ 400.0f), 420, 220);
			if(client.getConnectiontime() < 0) {
				System.exit(1);
			}
		}
		else{

			if(client.getCountdown() > 0){
				startCountDown = true;
				g.drawString("starting game in...", 350, 200);
				g.drawString("" + (int)(float)(client.getCountdown()/ 400.0f), 420, 220);
			}
		}
			}

	/** Get the friction coefficient of a map tile.
	 * @param x Map tile x coordinate (in tiles).
	 * @param y Map tile y coordinate (in tiles).
	 * @return Friction coefficient.
	 */
	public double tileFriction(int x, int y)
	{
		int tileid = map.getTileId(x, y, 0);
		String friction = map.getTileProperty(tileid, "friction", null);
		return Double.parseDouble(friction);
	}

	/** Determines whether a particular map tile blocks movement.
	 * @param x Map tile x coordinate (in tiles).
	 * @param y Map tile y coordinate (in tiles).
	 * @return true if the tile blocks movement.
	 */
	public boolean tileBlocks(int x, int y)
	{
		return tileFriction(x, y) >= 1;
	}

	/** Determines whether there is a unit at a particular coordinate.
	 * (Checks for a unit within the range of INTERACT_DISTANCE).
	 * @param x X coordinate (in pixels).
	 * @param y Y coordinate (in pixels).
	 * @param ignore Ignore this unit (usually the player). May be null.
	 * @return The unit at that coordinate, if there is one, else null.
	 */
	public Unit unitAt(double x, double y, Unit ignore)
	{
		for (Unit dest_unit : units)
		{
			double dist = dest_unit.distanceTo(x, y);
			if (dest_unit != ignore && dest_unit.isAlive()
					&& dist < INTERACT_DISTANCE)
			{
				return dest_unit;
			}
		}
		return null;
	}
}
