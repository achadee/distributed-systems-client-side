package comp90015.client;
/* COMP90015: Distributed Systems
 * Chadee Racing
 * Author: Avin Chadee <389017>, Jim Smith <286794>, Patrick Clarke <358519>
 */

import java.io.IOException;
import java.lang.reflect.Array;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import com.google.gson.Gson;

import comp90015.game.Game;
import comp90015.models.DataPacket;
import comp90015.models.PlayerMove;
import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;

public class ClientController{

	private DatagramSocket s;
	private InetAddress ip;

	public ClientController(DatagramSocket socket) throws UnknownHostException
	{
		this.ip = Game.getIp();
		this.s = socket;
	}

	public DatagramSocket getSocket() {
		return s;
	}

	private String gFeedback;

	public  DataPacket startCall(DataPacket dp) throws IOException{
		byte[] sendData = new byte[1024];
		byte[] receiveData = new byte[1024];

		String sr = ClientController.toJSON(dp);

		sendData = sr.getBytes();

		DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, this.ip, 1234);
		s.send(sendPacket);
		s.setSoTimeout(200);
		DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
		String str = null;
		
		try {
			s.receive(receivePacket);
			str = new String(receivePacket.getData());
		} catch (SocketTimeoutException ste) {
		    System.out.println("I timed out!");
		    return null;
		}
			

		return toObject(str);

		//System.out.println(str);

		
	}

	public String getFeedback() {
		return gFeedback;
	}

	public void setFeedback(String feedback) {
		gFeedback = feedback;
	}

	public static String convert_to_json(DataPacket info){
		Gson gson = new Gson();
		return gson.toJson(info);
	}

	public void close_socket() throws IOException{
		s.close();
	}

	public static String toJSON(DataPacket dp){
		return new JSONSerializer().deepSerialize(dp);
	}

	public static DataPacket toObject(String request)
	{
		@SuppressWarnings("rawtypes")
		DataPacket dp = (DataPacket) new JSONDeserializer().use("DataPacket.pm", Array.class).use("DataPacket.pm.values", PlayerMove.class).deserialize(request);
		return dp;
	}
}
