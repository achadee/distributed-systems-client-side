package comp90015.client;
/* COMP90015: Distributed Systems
 * Chadee Racing
 * Author: Avin Chadee <389017>, Jim Smith <286794>, Patrick Clarke <358519>
 */

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Vector;

import comp90015.game.Player;
import comp90015.game.Unit;
import comp90015.models.DataPacket;
import comp90015.models.PlayerMove;

public  class DataHandler {

	ClientController clientController;
	DatagramSocket clientSocket;

	public static int MAXPLAYERS = 4;
	private int playerId = 0;
	public DataPacket dp;
	public PlayerMove[] pm = new PlayerMove[MAXPLAYERS];
	public int timeoutCounter = 0;

	public DataHandler(DataPacket dp) throws IOException{
		this.dp = dp;
		this.dp.pm = this.pm;
		System.out.println(this.dp.pm);
		//this.dp.pm[0].id = 0;
		this.clientSocket = new DatagramSocket();
		this.clientController= new ClientController(clientSocket);

	}

	public void set_players(Vector<Player> units){
		for(int i = 0; i < units.size(); i++){
			pm[i] = new PlayerMove();
			Unit unit = units.elementAt(i);
			pm[i].setDegrees(unit.getAngle().getDegrees()); //might need to change to radians
			pm[i].setRotation(unit.getRotateSpeed());
			pm[i].setX(unit.getX());
			pm[i].setY(unit.getY());
			pm[i].setVelocity(unit.getSpeed());
		}
		dp.pm = pm;
	}

	public int getPlayerId() {
		return this.playerId;
	}


	public void set_play(Player unit, int i, double rotate_dir, double move_dir){
		pm[i].setDegrees(unit.getAngle().getDegrees()); //might need to change to radians
		pm[i].setRotation(rotate_dir);
		pm[i].setX(unit.getX());
		pm[i].setY(unit.getY());
		pm[i].setVelocity(move_dir);
		pm[i].setSpeed(unit.getSpeed());

		dp.pm[i] = pm[i];
	}

	//globally updates data
	public void DistributeData(DataPacket dp) throws IOException{
		this.dp.playerID = getPlayerId();
		this.dp.pm[getPlayerId()].id = this.pm[getPlayerId()].id + 1;
		this.dp.connected = true;
		DataPacket a = clientController.startCall(dp);
		if(a != null){
			this.dp = a;	
			timeoutCounter = 0;
		}
		else{
			timeoutCounter++;
		}
	}

	public int getConnectiontime() {
		return dp.connectionTime;
	}

	public void setConnectiontime(int connectiontime) {
		connectiontime = dp.connectionTime;
	}

	public int getCountdown() {
		return dp.countDownTime;
	}

	public void setCountdown(int countdown) {
		countdown = dp.countDownTime;
	}

	public int getTotalplayers() {
		return MAXPLAYERS;
	}

	public boolean GameStarted() {
		return dp.gameStarted;
	}

	public void closeSocket() {
		clientSocket.close();
	}
}
