package comp90015.client;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

import comp90015.models.DataPacket;

public class Listner implements Runnable{

	
	private DatagramSocket clientsocket;
	private DatagramPacket rp;
	private DataHandler dh;


	public Listner(DatagramSocket clientsocket, DataHandler dh){
		this.clientsocket = clientsocket;
		System.out.println("created Listner");
		this.dh = dh;
	}
	
	public DataPacket getPacket(){
		String str = new String(rp.getData());
		System.out.println(str);

		return ClientController.toObject(str);
	}
	
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		
		byte[] receiveData = new byte[1024];
		
		
		while (true) {
			DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
			try {
				clientsocket.receive(receivePacket);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			this.rp = receivePacket;
			try {
				dh.DistributeData(getPacket());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
}
