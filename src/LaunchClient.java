/* COMP90015: Distributed Systems
 * Chadee Racing
 * Author: Avin Chadee <389017>, Jim Smith <286794>, Patrick Clarke <358519>
 */

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import org.newdawn.slick.SlickException;

import comp90015.game.Game;

public class LaunchClient implements java.awt.event.KeyListener {

	public static void main(String[] args) throws Exception {
		
		/**
		 * Start the game
		 */
		
		
		
		try {
				Game.gameStart();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		if(e.getKeyChar()=='a'){
			System.out.println("Harro!");
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
}
