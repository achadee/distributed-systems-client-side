import cgi
import cgitb
import time
import shelve
import os
import json

seconds_b4_delete = 120
time_now = time.time()
dn = os.path.dirname(os.path.realpath(__file__))


def get():

	d = shelve.open(dn+"/servers", writeback=True)
	keys = d.keys()
	servdict = {}

	for key in keys:

		if (time_now - d[key][1]) > seconds_b4_delete:
			del d[key]
			d.sync()

		else:
			servdict[key] = d[key][0]

	if len(d.keys()) == 0:
		os.remove(dn+"/servers")

	d.close()
	
	return json.dumps(servdict)

def add(name, ip):

	d = shelve.open(dn+"/servers", writeback=True)
	d[ip] = (name, time_now)
	keys = d.keys()
	d.close()
	return "Added: " + str(keys)

def remove(ip):
	
	d = shelve.open(dn+"/servers", writeback=True)
	
	try:
		del d[ip]
	except:
		d.close()
		return "ERROR: IP to be deleted was not already registered with the lobby"

	keys= d.keys()
	d.close()
	return "Removed " + ip + " sucessfully."

def index(req):

	
	cgitb.enable()
	form = req.form.list

	req.content_type = 'application/json'


	args={}
	
	mode = "get"
	name = ""
	ip = ""

	#get args in dictionary form
	for f in form:
		args[f.name] = f.value

	if "name" in args:
		name = args["name"]

	if "ip" in args:
		ip = args["ip"]

	if "mode" in args:
		mode = args["mode"]


	#could check for valid inputs here
	#check mode here

	if mode == "get":
		return get()

	elif (mode == "add") and (ip != "") and (name != ""):
		return add(name, ip)

	elif (mode == "remove") and (ip != ""):
		return remove(ip)
	else:
		return "ERROR: Invalid mode or other arguments"



